<?php
function howQuadrantIsIt($x, $y) {
    if ($x > 0 && $y > 0) {
        return 'Первая четверть';
    } else if ($x < 0 && $y > 0) {
        return 'Вторая четверть';
    } else if ($x > 0 && $y < 0) {
        return 'Третья четверть';
    } else if ($x < 0 && $y < 0) {
        return 'Четвёртая четверть';
    };
};
$result = howQuadrantIsIt(2,3);
echo "<h2>$result</h2>";
?>
