<?php
function findDistanse($fpointCoordX, $fpointCoordY, $spointCoordX, $spointCoordY) {
    $fpcX = $fpointCoordX;
    $fpcY = $fpointCoordY;
    $spcX = $spointCoordX;
    $spcY = $spointCoordY;
    $distance = 0;
    if($fpcX == $spcX){
        if($fpcY > $spcY){
            $distance = $fpcY - $spcY;
        }else $distance = $spcY - $fpcY;
    }
    if($fpcY == $spcY){
        if($fpcX > $spcX){
            $distance = $fpcX - $spcX;
        }else $distance = $spcX - $fpcX;
    }
    if($fpcX != $spcX && $fpcY != $spcY){
        $distanceX = 0;
        $distanceY = 0;
        if($fpcY > $spcY){
            $distanceY = $fpcY - $spcY;
        }else $distanceY = $spcY - $fpcY;
        if($fpcX > $spcX){
            $distanceX = $fpcX - $spcX;
        }else $distanceX = $spcX - $fpcX;
        $summPowDist = pow($distanceX, 2) + pow($distanceY,2);
        $distance = sqrt($summPowDist);

    }
    return $distance;
};
$result = findDistanse(5, 5, 8, 2);
echo "<h2>$result</h2>";
?>