<?php
$arrStrNumbers = [0 => 'one', 1 => 'two', 2 => 'three', 3 => 'four', 4 => 'five',
                  5 => 'six', 6 => 'seven', 7 => 'eight', 8 => 'nine'];
$arrStrNumbersEndTeen = [0 => 'eleven', 1 => 'twelve', 2 => 'thirteen', 3 => 'fourteen', 4 =>'fifteen', 5 => 'sixteen',
                         6 => 'seventeen', 7 => 'eighteen', 8 => 'nineteen'];
$arrStrNumbersEndTy = [0 => 'ten', 1 => 'twenty', 2 => 'thirty', 3 => 'forty', 4 => 'fifty', 5 => 'sixty',
                       6 => 'seventy', 7 => 'eighty', 8 => 'ninety'];
function strInNumb($str) {
    global $arrStrNumbers,
           $arrStrNumbersEndTeen,
           $arrStrNumbersEndTy;
    $strLow = strtolower($str);
    $arr = explode(" ", $strLow);
    $helpNumb = 0;
    $mainNumb = 0;
    if(count($arr) == 4){
        if(!in_array($arr[0], $arrStrNumbers)) return 'Error';
        if($arr[1] != 'hundred'){
            return 'Error';
        };
        if(!in_array($arr[2], $arrStrNumbersEndTy)){
            return 'Error';
        };
        if(!in_array($arr[3], $arrStrNumbers)){
            return 'Error';
        };
        switch ($arr[0]) {
            case 'one': $mainNumb+= 100; break;
            case 'two': $mainNumb+= 200; break;
            case 'three': $mainNumb+= 300; break;
            case 'four': $mainNumb+= 400; break;
            case 'five': $mainNumb+= 500; break;
            case 'six': $mainNumb+= 600; break;
            case 'seven': $mainNumb+= 700; break;
            case 'eight': $mainNumb+= 800; break;
            case 'nine': $mainNumb+= 900; break;
            default: break;
        }
        switch ($arr[2]) {
            case 'twenty': $mainNumb+= 20; break;
            case 'thirty': $mainNumb+= 30; break;
            case 'forty': $mainNumb+= 40; break;
            case 'fifty': $mainNumb+= 50; break;
            case 'sixty': $mainNumb+= 60; break;
            case 'seventy': $mainNumb+= 70; break;
            case 'eighty': $mainNumb+= 80; break;
            case 'ninety': $mainNumb+= 90; break;
            default: break;
        }
        switch ($arr[3]) {
            case 'one': $mainNumb+= 1; break;
            case 'two': $mainNumb+= 2; break;
            case 'three': $mainNumb+= 3; break;
            case 'four': $mainNumb+= 4; break;
            case 'five': $mainNumb+= 5; break;
            case 'six': $mainNumb+= 6; break;
            case 'seven': $mainNumb+= 7; break;
            case 'eight': $mainNumb+= 8; break;
            case 'nine': $mainNumb+= 9; break;
            default: break;
        }
    }else if(count($arr) == 3) {
        if(!in_array($arr[0], $arrStrNumbers)){
            return 'Error';
        };
        if($arr[1] != 'hundred'){
            return 'Error';
        };
        if(!in_array($arr[2], $arrStrNumbers)){
            return 'Error';
        };
        switch ($arr[0]) {
            case 'one': $mainNumb+= 100; break;
            case 'two': $mainNumb+= 200; break;
            case 'three': $mainNumb+= 300; break;
            case 'four': $mainNumb+= 400; break;
            case 'five': $mainNumb+= 500; break;
            case 'six': $mainNumb+= 600; break;
            case 'seven': $mainNumb+= 700; break;
            case 'eight': $mainNumb+= 800; break;
            case 'nine': $mainNumb+= 900; break;
            default: break;
        }
        switch ($arr[2]) {
            case 'one': $mainNumb+= 1; break;
            case 'two': $mainNumb+= 2; break;
            case 'three': $mainNumb+= 3; break;
            case 'four': $mainNumb+= 4; break;
            case 'five': $mainNumb+= 5; break;
            case 'six': $mainNumb+= 6; break;
            case 'seven': $mainNumb+= 7; break;
            case 'eight': $mainNumb+= 8; break;
            case 'nine': $mainNumb+= 9; break;
            default: break;
        }
    }else
        if(count($arr) == 2){
            if(!in_array($arr[0], $arrStrNumbers)){
                if(arr[1] != 'hundred'){
                    return 'Error';
                };
            }else{
                if(!in_array($arr[0], $arrStrNumbersEndTy)){
                    if(!in_array($arr[1], $arrStrNumbers)){
                        return 'Error';
                    }
                }else{
                    return 'Error';
                };
            }
            if($arr[1] == 'hundred'){
                switch ($arr[0]) {
                    case 'one': $mainNumb+= 100; break;
                    case 'two': $mainNumb+= 200; break;
                    case 'three': $mainNumb+= 300; break;
                    case 'four': $mainNumb+= 400; break;
                    case 'five': $mainNumb+= 500; break;
                    case 'six': $mainNumb+= 600; break;
                    case 'seven': $mainNumb+= 700; break;
                    case 'eight': $mainNumb+= 800; break;
                    case 'nine': $mainNumb+= 900; break;
                    default: break;
                }
            }else{
                switch ($arr[0]) {
                    case 'twenty': $mainNumb+= 20; break;
                    case 'thirty': $mainNumb+= 30; break;
                    case 'forty': $mainNumb+= 40; break;
                    case 'fifty': $mainNumb+= 50; break;
                    case 'sixty': $mainNumb+= 60; break;
                    case 'seventy': $mainNumb+= 70; break;
                    case 'eighty': $mainNumb+= 80; break;
                    case 'ninety': $mainNumb+= 90; break;
                    default: break;
                }
                switch ($arr[1]) {
                    case 'one': $mainNumb+= 1; break;
                    case 'two': $mainNumb+= 2; break;
                    case 'three': $mainNumb+= 3; break;
                    case 'four': $mainNumb+= 4; break;
                    case 'five': $mainNumb+= 5; break;
                    case 'six': $mainNumb+= 6; break;
                    case 'seven': $mainNumb+= 7; break;
                    case 'eight': $mainNumb+= 8; break;
                    case 'nine': $mainNumb+= 9; break;
                    default: break;
                }
            }
        }else if(count($arr) == 1){
            if(!in_array($arr[0], $arrStrNumbers)){
                if(!in_array($arr[0], $arrStrNumbersEndTeen)) return 'Error';
            }
            switch ($arr[0]) {
                case 'one': $mainNumb+= 1; break;
                case 'two': $mainNumb+= 2; break;
                case 'three': $mainNumb+= 3; break;
                case 'four': $mainNumb+= 4; break;
                case 'five': $mainNumb+= 5; break;
                case 'six': $mainNumb+= 6; break;
                case 'seven': $mainNumb+= 7; break;
                case 'eight': $mainNumb+= 8; break;
                case 'nine': $mainNumb+= 9; break;
                case 'ten': $mainNumb+= 10; break;
                case 'eleven': $mainNumb+= 11; break;
                case 'twelve': $mainNumb+= 12; break;
                case 'thirteen': $mainNumb+= 13; break;
                case 'fourteen': $mainNumb+= 14; break;
                case 'fifteen': $mainNumb+= 15; break;
                case 'sixteen': $mainNumb+= 16; break;
                case 'seventeen': $mainNumb+= 17; break;
                case 'eighteen': $mainNumb+= 18; break;
                case 'nineteen': $mainNumb+= 19; break;
                default: break;
            }
        }
    return $mainNumb;
};
$result = strInNumb('Four hundred forty six');
echo "<h2>$result</h2>";
?>