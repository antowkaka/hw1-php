<?php
function howMuchDigit($numb){
    $one = $numb % 10;
    $ten = intval(($numb % 100) / 10);
    $hundred = intval(($numb % 1000) / 100);
    $thousand = intval(($numb % 1000000) / 1000);
    $million = intval(($numb % 1000000000) / 1000000);
    $billion = intval(($numb % 1000000000000) / 1000000000);
    return [0 => $one, 1 => $ten, 2 => $hundred, 3 => $thousand, 4 => $million, 5 => $billion];
}
function numbInString($num) {
    $str = "";
    $digit = howMuchDigit($num);
    switch ($digit[2]) {
        case 1:
            $str = $str . 'One hundred';
            break;
        case 2:
            $str = $str . 'Two hundred';
            break;
        case 3:
            $str = $str . 'Three hundred';
            break;
        case 4:
            $str = $str . 'Four hundred';
            break;
        case 5:
            $str = $str . 'Five hundred';
            break;
        case 6:
            $str = $str . 'Six hundred';
            break;
        case 7:
            $str = $str . 'Seven hundred';
            break;
        case 8:
            $str = $str . 'Eight hundred';
            break;
        case 9:
            $str = $str . 'Nine hundred';
            break;
        default:
            break;
    };
    if ($digit[1] == 1 && $digit[0] != 0) {
        switch (digit[0]) {
            case 1:
                $str = $str . ' eleven';
                break;
            case 2:
                $str = $str . ' twelve';
                break;
            case 3:
                $str = $str . ' thirteen';
                break;
            case 4:
                $str = $str . ' fourteen';
                break;
            case 5:
                $str = $str . ' fifteen';
                break;
            case 6:
                $str = $str . ' sixteen';
                break;
            case 7:
                $str = $str . ' seventeen';
                break;
            case 8:
                $str = $str . ' eighteen';
                break;
            case 9:
                $str = $str . ' nineteen';
                break;
            default:
                break;
        };
    } elseif ($digit[0] == 0) {
        switch ($digit[1]) {
            case 1:
                $str = $str . ' ten';
                break;
            case 2:
                $str = $str . ' twenty';
                break;
            case 3:
                $str = $str . ' thirty';
                break;
            case 4:
                $str = $str . ' forty';
                break;
            case 5:
                $str = $str . ' fifty';
                break;
            case 6:
                $str = $str . ' sixty';
                break;
            case 7:
                $str = $str . ' seventy';
                break;
            case 8:
                $str = $str . ' eighty';
                break;
            case 9:
                $str = $str . ' ninety';
                break;
            default:
                break;
        };
    } else {
        switch ($digit[1]) {
            case 1:
                $str = $str . ' ten';
                break;
            case 2:
                $str = $str . ' twenty';
                break;
            case 3:
                $str = $str . ' thirty';
                break;
            case 4:
                $str = $str . ' forty';
                break;
            case 5:
                $str = $str . ' fifty';
                break;
            case 6:
                $str = $str . ' sixty';
                break;
            case 7:
                $str = $str . ' seventy';
                break;
            case 8:
                $str = $str . ' eighty';
                break;
            case 9:
                $str = $str . ' ninety';
                break;
            default:
                break;
        };
        switch ($digit[0]) {
            case 1:
                $str = $str . ' one';
                break;
            case 2:
                $str = $str . ' two';
                break;
            case 3:
                $str = $str . ' three';
                break;
            case 4:
                $str = $str . ' four';
                break;
            case 5:
                $str = $str . ' five';
                break;
            case 6:
                $str = $str . ' six';
                break;
            case 7:
                $str = $str . ' seven';
                break;
            case 8:
                $str = $str . ' eight';
                break;
            case 9:
                $str = $str . ' nine';
                break;
            default:
                break;
        };
    };
    return $str;
};
$result = numbInString(245);
echo "<h2>$result</h2>";
?>