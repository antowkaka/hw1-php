<?php
function dayOfWeek($numbDay){
    $week = [
        1 => 'Monday',
        2 => 'Tuesday',
        3 => 'Wednesday',
        4 => 'Thursday',
        5 => 'Friday',
        6 => 'Saturday',
        7 => 'Sunday'
    ];
    foreach($week as $k => $v){
        if($numbDay == $k){
            return $v;
        };
    };
};
$result = dayOfWeek(2);
echo "<h2>$result</h2>";
?>