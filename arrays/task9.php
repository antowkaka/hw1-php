<?php
$arr = [5, 9, 7, 3, 4, 7, 2, 9, 5, 6, 3, 6];
//Bubble sort
function bubbleSort($arr){
    $x = 0;
    $swapped = true;
    while($swapped == true){
        $swapped = false;
        for($i = 0; $i < count($arr) - 1; $i++){
            if($arr[$i] > $arr[$i + 1]){
                $x = $arr[$i + 1];
                $arr[$i + 1] = $arr[$i];
                $arr[$i] = $x;
                $swapped = true;
            };
        };
    };
    return $arr;
};

//Insert sort
function insertSort($arr){
    $x = 0;
    for($i = 0; $i < count($arr); $i++){
        for($j = $i + 1; $j < count($arr); $j++){
            if($arr[$i] > $arr[$j]){
                $x = $arr[$j];
                $arr[$j] = $arr[$i];
                $arr[$i] = $x;
            };
        };
    };
    return $arr;
};

//Select sort
function selectSort($arr){
    $x = 0;
    for($i = 0; $i < count($arr); $i++){
        $min = $i;
        for($j = $i + 1; $j < count($arr); $j++){
            if($arr[$min] > $arr[$j]){
                $min = $j;
            };
        };
        $x = $arr[$min];
        $arr[$min] = $arr[$i];
        $arr[$i] = $x;
    };
    return $arr;
};
$resultBubble = bubbleSort($arr);
$resultInsert = insertSort($arr);
$resultSelect = selectSort($arr);
echo "<h2>Bubble Sort</h2>";
echo print_r($resultBubble);
echo "<h2>Insert Sort</h2>";
echo print_r($resultInsert);
echo "<h2>Select Sort</h2>";
echo print_r($resultSelect);
?>