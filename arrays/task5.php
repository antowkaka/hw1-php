<?php
$arr = [5, 9, 7, 3, 4, 7, 2, 9, 5, 6, 3, 6];
function summOddDigits($arr){
    $summ = 0;
    for($i = 0; $i < count($arr); $i++){
        if($arr[$i] % 2 != 0){
            $summ += $arr[$i];
        };
    };
    return $summ;
};
$result = summOddDigits($arr);
echo "<h2>$result</h2>";
?>