<?php
$arr = [5, 9, 7, 3, 4, 7, 2, 9, 5, 6, 3, 6];
function minElem($arr) {
    $x = 0;
    for($i = 0; $i < count($arr); $i++){
        for($j = $i+1; $j < count($arr); $j++){
            if($arr[$i] > $arr[$j]){
                $x = $arr[$j];
                $arr[$j] = $arr[$i];
                $arr[$i] = $x;
            }
        };
    };
    return $arr[0];
};
$result = minElem($arr);
echo "<h2>$result</h2>";
?>